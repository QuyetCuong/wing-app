import React from "react";
import { Slide } from "react-slideshow-image";
import "react-slideshow-image/dist/styles.css";
import imageSlide1 from "../../assets/img/slide/img-slide-1.jpg";
import imageSlide2 from "../../assets/img/slide/img-slide-2.jpg";
import imageSlide3 from "../../assets/img/slide/img-slide-3.jpg";

const listSlide = [imageSlide1, imageSlide2, imageSlide3];

function Home() {
  return (
    <div className="div-container">
      <div className="slide-container">
        <Slide duration="500">
          {listSlide.map((item, index) => {
            return (
              <div className="each-slide">
                <img className="img-slide" src={item} alt=""/>;
              </div>
            );
          })}
        </Slide>
      </div>
    </div>
  );
}

export default Home;
