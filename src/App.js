import React from "react";
import "./App.css";
import Navbar from "./components/navbar/Navbar";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/home/Home";
import Reports from "./pages/Reports";
import Products from "./pages/Products";
import MenuBar from "./components/menu-bar/MenuBar";
import { useMediaQuery } from "react-responsive";

function App() {
  const isMobile = useMediaQuery({ query: `(max-width: 760px)` });

  return (
    <>
      <Router>
        {isMobile ? <Navbar /> : <MenuBar />}
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/reports" component={Reports} />
          <Route path="/products" component={Products} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
