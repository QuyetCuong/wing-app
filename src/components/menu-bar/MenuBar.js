import React from "react";
import "./MenuBar.css";
import { SidebarData } from "../SidebarData";
import { Link } from "react-router-dom";
import logo from "../../assets/img/logo.PNG";

function MenuBar() {
  return (
    <>
      <div className="div-menu">
        <div className="div-avatar">
          <img className="logo" src={logo} alt=""/>
        </div>
        <ul className="ulSlideBar">
          {SidebarData.map((item, index) => {
            return (
              <li key={index} className="liSlideBar">
                <Link to={item.path}>
                  <span>{item.title}</span>
                </Link>
              </li>
            );
          })}
        </ul>
      </div>
    </>
  );
}

export default MenuBar;
