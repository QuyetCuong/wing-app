export const SidebarData = [
  {
    title: 'Trang chủ',
    path: '/',
    cName: 'nav-text'
  },
  {
    title: 'Vang cao cấp',
    path: '/reports',
    cName: 'nav-text'
  },
  {
    title: 'Rượu vang',
    path: '/products',
    cName: 'nav-text'
  },
  {
    title: 'Vang bịch',
    path: '/team',
    cName: 'nav-text'
  },
  {
    title: 'Champane',
    path: '/messages',
    cName: 'nav-text'
  },
  {
    title: 'Liên hệ',
    path: '/support',
    cName: 'nav-text'
  }
];
